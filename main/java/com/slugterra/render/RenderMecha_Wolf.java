package com.slugterra.render;

import com.slugterra.entity.EntityMecha;
import com.slugterra.lib.Strings;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderMecha_Wolf extends RenderLiving
{
    private static final ResourceLocation mechaTextures = new ResourceLocation(Strings.MODID + ":textures/entity/mecha_wolf.png");
    private static final String __OBFID = "CL_00000984";

    public RenderMecha_Wolf(ModelBase p_i1253_1_, float p_i1253_2_)
    {
        super(p_i1253_1_, p_i1253_2_);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(EntityMecha p_110775_1_)
    {
        return mechaTextures;
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(Entity p_110775_1_)
    {
        return this.getEntityTexture((EntityMecha)p_110775_1_);
    }
}