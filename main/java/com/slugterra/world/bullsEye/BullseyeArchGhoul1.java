package com.slugterra.world.bullsEye;
import java.util.Random;

import net.minecraft.init.Blocks;
import net.minecraft.world.World;
public class BullseyeArchGhoul1 {
	public boolean generate(World world, Random par2Random, int x, int y, int z) {
		world.setBlock(x + 0, y + 0, z + 2, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 0, z + 4, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 0, z + 4, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 3, y + 0, z + 4, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 4, y + 0, z + 4, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 5, y + 0, z + 4, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 1, y + 0, z + 5, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 6, y + 0, z + 5, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 0, y + 0, z + 6, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 7, y + 0, z + 6, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 0, y + 0, z + 7, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 7, y + 0, z + 7, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 0, y + 0, z + 8, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 6, y + 0, z + 8, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 7, y + 0, z + 8, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 0, y + 0, z + 9, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 1, y + 0, z + 9, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 0, z + 9, Blocks.wool, 10, 1);
		world.setBlock(x + 7, y + 0, z + 9, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 1, y + 0, z + 10, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 2, y + 0, z + 10, Blocks.wool, 10, 1);
		world.setBlock(x + 3, y + 0, z + 10, Blocks.wool, 10, 1);
		world.setBlock(x + 6, y + 0, z + 10, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 1, y + 0, z + 11, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 0, z + 11, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 3, y + 0, z + 11, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 4, y + 0, z + 11, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 5, y + 0, z + 11, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 0, y + 0, z + 13, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 1, z + 1, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 1, z + 3, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 1, z + 5, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 3, y + 1, z + 5, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 4, y + 1, z + 5, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 5, y + 1, z + 5, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 1, y + 1, z + 6, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 2, y + 1, z + 6, Blocks.wool, 10, 1);
		world.setBlock(x + 6, y + 1, z + 6, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 1, y + 1, z + 7, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 6, y + 1, z + 7, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 1, y + 1, z + 8, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 6, y + 1, z + 8, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 1, y + 1, z + 9, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 2, y + 1, z + 9, Blocks.wool, 10, 1);
		world.setBlock(x + 3, y + 1, z + 9, Blocks.wool, 10, 1);
		world.setBlock(x + 4, y + 1, z + 9, Blocks.wool, 10, 1);
		world.setBlock(x + 5, y + 1, z + 9, Blocks.wool, 10, 1);
		world.setBlock(x + 6, y + 1, z + 9, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 2, y + 1, z + 10, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 3, y + 1, z + 10, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 4, y + 1, z + 10, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 5, y + 1, z + 10, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 1, y + 1, z + 12, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 1, z + 14, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 2, z + 0, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 2, z + 2, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 2, z + 4, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 2, z + 6, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 3, y + 2, z + 6, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 4, y + 2, z + 6, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 5, y + 2, z + 6, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 2, y + 2, z + 7, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 3, y + 2, z + 7, Blocks.wool, 10, 1);
		world.setBlock(x + 4, y + 2, z + 7, Blocks.wool, 10, 1);
		world.setBlock(x + 5, y + 2, z + 7, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 2, y + 2, z + 8, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 3, y + 2, z + 8, Blocks.wool, 10, 1);
		world.setBlock(x + 5, y + 2, z + 8, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 2, y + 2, z + 9, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 3, y + 2, z + 9, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 4, y + 2, z + 9, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 5, y + 2, z + 9, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 2, y + 2, z + 11, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 2, z + 13, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 2, z + 15, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 3, z + 0, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 3, z + 2, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 3, z + 4, Blocks.wool, 10, 1);
		world.setBlock(x + 3, y + 3, z + 7, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 4, y + 3, z + 7, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 3, y + 3, z + 8, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 4, y + 3, z + 8, Blocks.nether_brick, 0, 1);
		world.setBlock(x + 2, y + 3, z + 11, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 3, z + 13, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 3, z + 15, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 4, z + 0, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 4, z + 2, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 4, z + 4, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 4, z + 11, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 4, z + 13, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 4, z + 15, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 5, z + 0, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 5, z + 2, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 5, z + 4, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 5, z + 11, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 5, z + 13, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 5, z + 15, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 6, z + 0, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 6, z + 2, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 6, z + 5, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 6, z + 10, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 6, z + 13, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 6, z + 15, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 7, z + 1, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 7, z + 3, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 7, z + 6, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 7, z + 7, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 7, z + 8, Blocks.wool, 10, 1);
		world.setBlock(x + 2, y + 7, z + 9, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 7, z + 12, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 7, z + 14, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 8, z + 1, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 8, z + 4, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 8, z + 11, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 8, z + 14, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 9, z + 2, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 9, z + 5, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 9, z + 6, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 9, z + 7, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 9, z + 8, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 9, z + 9, Blocks.wool, 10, 1);
		world.setBlock(x + 1, y + 9, z + 10, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 9, z + 13, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 10, z + 3, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 10, z + 4, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 10, z + 11, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 10, z + 12, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 11, z + 5, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 11, z + 6, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 11, z + 7, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 11, z + 8, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 11, z + 9, Blocks.wool, 10, 1);
		world.setBlock(x + 0, y + 11, z + 10, Blocks.wool, 10, 1);
		return true;
	}
}
