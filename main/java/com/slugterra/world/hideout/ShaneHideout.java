package com.slugterra.world.hideout;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
public class ShaneHideout extends WorldGenerator {

	public boolean generate(World par1World, Random par2Random, int par3, int par4, int par5) {
		new ShaneHideout1().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout2().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout3().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout4().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout5().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout6().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout7().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout8().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout9().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout10().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout11().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout12().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout13().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout14().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout15().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout16().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout17().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout18().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout19().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout20().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout21().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout22().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout23().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout24().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout25().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout26().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout27().generate(par1World, par2Random, par3, par4, par5);
		new ShaneHideout28().generate(par1World, par2Random, par3, par4, par5);
		return true;
	} 
}
